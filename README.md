# Selling Migration Services

This project will enable partners with the information they need to effectively sell migration services to customers. This includes the process of 
- discovery,
- positioning,
- gathering required information,
- creating proposals,
- generating SOWs,
- Establishing MSAs,
- Paperwork process to execute an engagement


[This presentation](https://docs.google.com/presentation/d/1Kuvd9aVPbaRbdTyQq0tLIzrRfaCHt1TF1kr2_eJukzI/edit?usp=sharing) was used to help establish the standard process of selling GitLab services. 

Bryan May from the professional services team explains the value of professional services and how to position migration services to a customer in the [this video](https://www.loom.com/share/952efc344c094651b2c696ea2caea8d0) 

Bryan May from the professional services team explains the selling process once the opportunity is qualified in [this video](https://www.loom.com/share/952efc344c094651b2c696ea2caea8d0)
